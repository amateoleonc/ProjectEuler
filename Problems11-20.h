//
// Created by amate on 6/02/2021.
//

#ifndef PROJECTEULER_PROBLEMS11_20_H
#define PROJECTEULER_PROBLEMS11_20_H

#include "Problems1-10.h"

vector<int> stringToMatrix (string s1);

long largestProductGrid ();

#endif //PROJECTEULER_PROBLEMS11_20_H
