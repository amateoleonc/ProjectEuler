#include "Problems11-20.h"

long largestProductGrid () { //Find the largest product in a 20x20 matrix
    int arr[20][20];
    for (auto & i : arr) {
        for (int & j : i) {
            cin >> j;
        }
    }
    long max = 0;
    for (auto & i : arr) { //check rows
        for (int j = 0; j < 16; ++j) {
            if (i[j] * i[j + 1] * i[j + 2] * i[j + 3] > max) {
                max = i[j] * i[j + 1] * i[j + 2] * i[j + 3];
            }
        }
    }
    for (int i = 0; i < 16; ++i) { //check columns
        for (int j = 0; j < 20; ++j) {
            if (arr[i][j] * arr[i][j + 1] * arr[i][j + 2] * arr[i][j + 3] > max) {
                max = arr[i][j] * arr[i][j + 1] * arr[i][j + 2] * arr[i][j + 3];
            }
        }
    }
    for (int i = 0; i < 17; ++i) { //check left to right up to down diagonals
        for (int j = 0; j < 17; ++j) {
            if (arr[i][j] * arr[i + 1][j + 1] * arr[i + 2][j + 2] * arr[i + 3][j + 3] > max) {
                max = arr[i][j] * arr[i + 1][j + 1] * arr[i + 2][j + 2] * arr[i + 3][j + 3];
            }
        }
    }
    for (int i = 3; i < 20; ++i) { //check remaining diagonals
        for (int j = 0; j < 17; ++j) {
            if (arr[i][j] * arr[i - 1][j + 1] * arr[i - 2][j + 2] * arr[i - 3][j + 3] > max) {
                max = arr[i][j] * arr[i - 1][j + 1] * arr[i - 2][j + 2] * arr[i - 3][j + 3];
            }
        }
    }
    return max;
}